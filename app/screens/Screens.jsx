import React from "react";
import Home from "../home/Home";
import Characters from "../characters/Characters";
import Episodes from "../episodes/Episodes";
import BackGroundScreen from "../commons/BackGroundScreen";

export const HomeWithScreen = () => (
  <BackGroundScreen>
    <Home />
  </BackGroundScreen>
);

export const CharactersWithScreen = () => (
  <BackGroundScreen>
    <Characters />
  </BackGroundScreen>
);

export const EpisodesWithScreen = () => (
  <BackGroundScreen>
    <Episodes />
  </BackGroundScreen>
);

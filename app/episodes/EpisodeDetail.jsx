import React from "react";
import { StyleSheet, Text, View } from "react-native";
import PropTypes from "prop-types";
import CharactersList from "../commons/CharactersList";
import BackGroundScreen from "../commons/BackGroundScreen";
import { getEpisode } from "../commons/Fetch";

const styles = StyleSheet.create({
  card: {
    alignItems: "center",
    backgroundColor: "rgba(104, 110, 116, 0.5)",
    borderRadius: 15,
    color: "#ffffff",
    flex: 1,
    height: "80%",
    justifyContent: "center",
    margin: "6%",
  },
  container: {
    flex: 1,
  },
  image: {
    borderRadius: 10,
    height: 250,
    marginTop: "6%",
    width: 250,
  },
  text: {
    color: "#ffffff",
    fontSize: 20,
    fontWeight: "bold",
    opacity: 1,
  },
});

export default class EpisodeDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    const {
      route: {
        params: { id },
      },
    } = this.props;
    const data = await getEpisode(id);
    this.setState({ ...data });
  };

  render() {
    const { name, airDate, characters, episode } = this.state;
    return (
      <BackGroundScreen>
        {name && (
          <View style={styles.card}>
            <Text style={styles.text}> {name}</Text>
            <Text style={styles.text}> {airDate}</Text>
            <Text style={styles.text}> {episode}</Text>
            <CharactersList characters={characters} />
          </View>
        )}
      </BackGroundScreen>
    );
  }
}

EpisodeDetail.propTypes = {
  route: PropTypes.object,
};

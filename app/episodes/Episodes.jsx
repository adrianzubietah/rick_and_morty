import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from "react-native";
import Constants from "expo-constants";
import { useNavigation } from "@react-navigation/native";
import PropTypes from "prop-types";
import { getEpisodes } from "../commons/Fetch";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    marginTop: Constants.statusBarHeight,
  },
  item: {
    alignItems: "center",
    backgroundColor: "rgba(104, 110, 116, 0.5)",
    borderRadius: 15,
    flex: 1,
    justifyContent: "center",
    margin: "1%",
    marginVertical: 8,
    padding: "5%",
    width: "45%",
  },
  title: {
    color: "white",
    fontSize: 15,
    textAlign: "center",
  },
});

class Episodes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      nextPage: "https://rickandmortyapi.com/api/episode?page=0",
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    const { nextPage } = this.state;
    const data = await getEpisodes(nextPage);
    const {
      results,
      info: { next },
    } = data;
    this.setState((prevState) => ({
      nextPage: next,
      items: [...prevState.items, ...results],
    }));
  };

  renderItem = ({ item }) => (
    <TouchableOpacity
      style={styles.item}
      onPress={() => {
        const { navigation } = this.props;
        return navigation.navigate("EpisodeDetail", { id: item.id });
      }}
    >
      <Text style={styles.title}> {item.name} </Text>
    </TouchableOpacity>
  );

  render() {
    const { items } = this.state;
    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          renderItem={this.renderItem}
          keyExtractor={(item) => item.id}
          numColumns={2}
          onEndReached={this.getData}
          onEndReachedThreshold={0.5}
        />
      </View>
    );
  }
}

Episodes.propTypes = {
  navigation: PropTypes.object,
};

export default function (props) {
  const navigation = useNavigation();
  return <Episodes {...props} navigation={navigation} />;
}

import React from "react";
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    width: "100%",
  },
  image: {
    borderRadius: 10,
    height: 130,
    width: 130,
  },
  item: {
    borderColor: "white",
    borderRadius: 11,
    borderStyle: "solid",
    borderWidth: 1,
    color: "white",
    flex: 1,
    justifyContent: "center",
    margin: "5%",
    width: "18%",
  },
  text: {
    color: "white",
    flex: 1,
    textAlign: "center",
  },
});

class CharactersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      episodesIds: [],
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    const { characters } = this.props;
    const episodesIds = characters.map((episode) =>
      episode.replace("https://rickandmortyapi.com/api/character/", "")
    );

    this.setState({
      episodesIds,
    });
  };

  render() {
    const { navigation } = this.props;
    const { episodesIds } = this.state;
    return (
      !!episodesIds.length && (
        <View style={styles.container}>
          <FlatList
            data={episodesIds}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={styles.item}
                onPress={() => navigation.push("CharacterDetail", { id: item })}
              >
                <Image
                  source={{
                    uri: `https://rickandmortyapi.com/api/character/avatar/${item}.jpeg`,
                  }}
                  style={styles.image}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item) => item}
            numColumns={2}
          />
        </View>
      )
    );
  }
}

CharactersList.propTypes = {
  characters: PropTypes.array,
  navigation: PropTypes.object,
};

const CharactersListNavigation = (props) => {
  const navigation = useNavigation();
  return <CharactersList {...props} navigation={navigation} />;
};

export default CharactersListNavigation;

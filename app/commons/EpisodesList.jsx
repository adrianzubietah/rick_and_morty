import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    width: "100%",
  },
  item: {
    borderColor: "white",
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 1,
    color: "white",
    flex: 1,
    justifyContent: "center",
    margin: "5%",
    width: "18%",
  },
  text: {
    color: "white",
    flex: 1,
    textAlign: "center",
  },
});

const EpisodesList = ({ episodes, navigation }) => {
  const episodesIds = episodes.map((episode) =>
    episode.replace("https://rickandmortyapi.com/api/episode/", "")
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={episodesIds}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={styles.item}
            onPress={() => {
              navigation.push("EpisodeDetail", {
                id: item,
              });
            }}
          >
            <Text style={styles.text}>{item}</Text>
          </TouchableOpacity>
        )}
        keyExtractor={(item) => item}
        numColumns={5}
      />
    </View>
  );
};

EpisodesList.propTypes = {
  episodes: PropTypes.array,
  navigation: PropTypes.object,
};

const EpisodesListNavigation = (props) => {
  const navigation = useNavigation();
  return <EpisodesList {...props} navigation={navigation} />;
};

export default EpisodesListNavigation;

import axios from "axios";

export const getEpisode = async (id) => {
  const response = await axios.get(
    `https://rickandmortyapi.com/api/episode/${id}`
  );
  const {
    data: { name, air_date: airDate, characters, episode },
  } = response;
  return { name, airDate, characters, episode };
};

export const getEpisodes = async (nextPage) => {
  const response = await axios.get(nextPage);
  const { data } = response;
  return data;
};

export const getCharater = async (id) => {
  const response = await axios.get(
    `https://rickandmortyapi.com/api/character/${id}`
  );
  const { data } = response;
  return data;
};

export const getCharaters = async (nextPage) => {
  const response = await axios.get(nextPage);
  const { data } = response;
  return data;
};

import React from "react";
import { StyleSheet, View, ImageBackground } from "react-native";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
});

const BackGroundScreen = ({ children }) => {
  return (
    <View style={styles.flex}>
      <ImageBackground
        source={require("../../assets/background.png")}
        style={styles.flex}
      >
        {children}
      </ImageBackground>
    </View>
  );
};

BackGroundScreen.propTypes = {
  children: PropTypes.any,
};

export default BackGroundScreen;

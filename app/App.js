import React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CharacterDetail from "./characters/CharacterDetail";
import EpisodeDetail from "./episodes/EpisodeDetail";
import {
  HomeWithScreen,
  CharactersWithScreen,
  EpisodesWithScreen,
} from "./screens/Screens";

const Stack = createStackNavigator();
const { Navigator, Screen } = Stack;

const styles = StyleSheet.create({
  backGround: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
});

const App = () => {
  return (
    <NavigationContainer style={styles.container}>
      <Navigator>
        <Screen
          name="Home"
          component={HomeWithScreen}
          options={{
            headerShown: false,
          }}
        />
        <Screen
          name="Characters"
          component={CharactersWithScreen}
          options={{
            headerShown: false,
          }}
        />
        <Screen
          name="CharacterDetail"
          component={CharacterDetail}
          options={{
            headerShown: false,
          }}
        />
        <Screen
          name="Episodes"
          component={EpisodesWithScreen}
          options={{
            headerShown: false,
          }}
        />
        <Screen
          name="EpisodeDetail"
          component={EpisodeDetail}
          options={{
            headerShown: false,
          }}
        />
      </Navigator>
    </NavigationContainer>
  );
};

export default App;

import React from "react";
import { StyleSheet, View } from "react-native";
import Constants from "expo-constants";
import { Entypo, MaterialCommunityIcons, Octicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import PropTypes from "prop-types";
import Card from "./component/Card";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around",
    paddingTop: Constants.statusBarHeight,
  },
  icon: {
    color: "#ffffff",
    fontSize: 50,
  },
  textContainer: {
    borderColor: "grey",
    borderStyle: "solid",
    borderWidth: 1,
    color: "#ffffff",
    flex: 1,
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    textAlignVertical: "center",
  },
});

const Home = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Card
        name="Characters"
        icon={<Octicons name="person" style={styles.icon} />}
        onPress={() => navigation.navigate("Characters")}
      />
      <Card
        name="Locations"
        icon={<Entypo name="location" style={styles.icon} />}
        onPress={() => navigation.navigate("Characters")}
      />
      <Card
        name="Episodes"
        icon={
          <MaterialCommunityIcons
            name="television-classic"
            style={styles.icon}
          />
        }
        onPress={() => navigation.navigate("Episodes")}
      />
    </View>
  );
};

Home.propTypes = {
  navigation: PropTypes.object,
};

export default function (props) {
  const navigation = useNavigation();
  return <Home {...props} navigation={navigation} />;
}

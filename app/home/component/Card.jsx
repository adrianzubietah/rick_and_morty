import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  card: {
    alignItems: "center",
    backgroundColor: "rgba(104, 110, 116, 0.5)",
    borderRadius: 15,
    color: "#ffffff",
    flex: 1,
    justifyContent: "center",
    margin: "6%",
  },
  container: {
    flex: 1,
  },
  text: {
    color: "#ffffff",
    fontSize: 30,
    fontWeight: "bold",
    opacity: 1,
  },
});

const Card = ({ name, icon, onPress }) => {
  return (
    <TouchableOpacity style={styles.card} onPress={onPress}>
      {icon}
      <Text style={styles.text}> {name}</Text>
    </TouchableOpacity>
  );
};

Card.propTypes = {
  name: PropTypes.string,
  icon: PropTypes.any,
  onPress: PropTypes.func,
};

export default Card;

import React from "react";
import {
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
} from "react-native";
import Constants from "expo-constants";
import { useNavigation } from "@react-navigation/native";
import PropTypes from "prop-types";
import { getCharaters } from "../commons/Fetch";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    marginTop: Constants.statusBarHeight,
  },
  image: {
    borderRadius: 10,
    height: 150,
    width: 150,
  },
  item: {
    alignItems: "center",
    backgroundColor: "rgba(104, 110, 116, 0.5)",
    borderRadius: 15,
    justifyContent: "center",
    margin: "1%",
    marginVertical: 8,
    padding: "2%",
    width: "45%",
  },
  title: {
    color: "white",
    fontSize: 18,
  },
});

class Charaters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      nextPage: "https://rickandmortyapi.com/api/character/?page=0",
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    const { nextPage } = this.state;
    const data = await getCharaters(nextPage);
    const {
      results,
      info: { next },
    } = data;
    this.setState((prevState) => ({
      nextPage: next,
      items: [...prevState.items, ...results],
    }));
  };

  renderItem = ({ item: { id, name, image } }) => {
    const { navigation } = this.props;
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={() => navigation.navigate("CharacterDetail", { id })}
      >
        <Image source={{ uri: image }} style={styles.image} />
        <Text style={styles.title}> {name} </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { items } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          data={items}
          renderItem={this.renderItem}
          keyExtractor={(item) => item.id}
          numColumns={2}
          onEndReached={this.getData}
          onEndReachedThreshold={0.5}
        />
      </SafeAreaView>
    );
  }
}

Charaters.propTypes = {
  navigation: PropTypes.object,
};

export default function (props) {
  const navigation = useNavigation();
  return <Charaters {...props} navigation={navigation} />;
}

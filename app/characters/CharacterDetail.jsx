import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import PropTypes from "prop-types";
import EpisodesList from "../commons/EpisodesList";
import BackGroundScreen from "../commons/BackGroundScreen";
import { getCharater } from "../commons/Fetch";

const styles = StyleSheet.create({
  card: {
    alignItems: "center",
    backgroundColor: "rgba(104, 110, 116, 0.5)",
    borderRadius: 15,
    color: "#ffffff",
    flex: 1,
    height: "80%",
    justifyContent: "center",
    margin: "6%",
  },
  container: {
    flex: 1,
  },
  image: {
    borderRadius: 10,
    height: 250,
    marginTop: "6%",
    width: 250,
  },
  text: {
    color: "#ffffff",
    fontWeight: "bold",
    opacity: 1,
  },
});

export default class CharaterDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    const {
      route: {
        params: { id },
      },
    } = this.props;
    const data = await getCharater(id);
    this.setState({ ...data });
  }

  render() {
    const {
      gender,
      image,
      name,
      species,
      status,
      type,
      location,
      origin,
      episode,
    } = this.state;
    return (
      <BackGroundScreen>
        {name && (
          <View style={styles.card}>
            <Image source={{ uri: image }} style={styles.image} />
            <Text style={styles.text}> {name}</Text>
            <Text style={styles.text}> {gender}</Text>
            <Text style={styles.text}> {species}</Text>
            <Text style={styles.text}> {status}</Text>
            <Text style={styles.text}> {type}</Text>
            <Text style={styles.text}> Location: {location.name}</Text>
            <Text style={styles.text}> Origin: {origin.name}</Text>
            <Text style={styles.text}> Episodes </Text>
            <EpisodesList episodes={episode} />
          </View>
        )}
      </BackGroundScreen>
    );
  }
}

CharaterDetail.propTypes = {
  route: PropTypes.object,
};
